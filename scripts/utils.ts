import { readFile, writeFile } from "node:fs/promises";
import path from "node:path";
import is from "@sindresorhus/is";
import Color from "colorjs.io";
import type { CustomProperty, RGBColor } from "lightningcss";

import type { Tokens, WithRequired } from "./types";

export async function readStylesheet(stylesheet: string): Promise<Buffer> {
  const inputPath = new URL(path.join("..", "node_modules", stylesheet), import.meta.url);
  const content = await readFile(inputPath);

  return content;
}

export async function writeTokens(outputFilename: string, tokens: object): Promise<void> {
  const outputPath = new URL(path.join("..", "dtcg", outputFilename), import.meta.url);
  await writeFile(outputPath, JSON.stringify(tokens));
}

export function rgbToHex(color: RGBColor): string {
  const { r, g, b, alpha } = color;
  const parsedColor = new Color(`rgba(${r} ${g} ${b} / ${alpha})`);

  return parsedColor.toString({ format: "hex", collapse: false });
}

export function removePrefix(value: string, prefix: string): string {
  return value.startsWith(prefix) ? value.slice(prefix.length) : value;
}

export function removeCssVarPrefix(value: string, prefix: string): string {
  const fullPrefix = `--${prefix}-`;
  return removePrefix(value, fullPrefix);
}

export function initColors(): WithRequired<Tokens, "colors"> {
  return {
    colors: {
      $type: "color",
    },
  };
}

export function initDimensions(): WithRequired<Tokens, "dimensions"> {
  return {
    dimensions: {
      $type: "dimension",
    },
  };
}

export async function fetchStylesheet(stylesheet: URL): Promise<Buffer> {
  const response = await fetch(stylesheet);
  const content = await response.text();

  return Buffer.from(content);
}

export function extractRgbColor(node: CustomProperty): RGBColor | null {
  if (
    node.value.length === 1 &&
    node.value[0].type === "color" &&
    is.object(node.value[0].value) &&
    node.value[0].value.type === "rgb"
  ) {
    return node.value[0].value;
  }

  return null;
}
