export type WithRequired<T, K extends keyof T> = Omit<T, K> & Required<Pick<T, K>>;

type TokenValue = {
  $value: string;
};

export type Tokens = {
  colors?: {
    $type: "color";
  };
  dimensions?: {
    $type: "dimension";
    border?: {
      radius?: {
        [key: string]: TokenValue;
      };
      width?: {
        [key: string]: TokenValue;
      };
    };
  };
};
