import defaultTheme from "tailwindcss/defaultTheme";

import { initDimensions, writeTokens } from "./utils";

async function main() {
  const tokens = initDimensions();

  for (const borderRadius in defaultTheme.borderRadius) {
    const value = defaultTheme.borderRadius[borderRadius];

    tokens.dimensions = {
      ...tokens.dimensions,
      border: {
        ...tokens.dimensions.border,
        radius: {
          ...tokens.dimensions.border?.radius,
          [borderRadius]: { $value: value },
        },
      },
    };
  }

  for (const borderWidth in defaultTheme.borderWidth) {
    const value = defaultTheme.borderWidth[borderWidth];

    tokens.dimensions = {
      ...tokens.dimensions,
      border: {
        ...tokens.dimensions.border,
        width: {
          ...tokens.dimensions.border?.width,
          [borderWidth]: { $value: value },
        },
      },
    };
  }

  await writeTokens("tailwind.tokens.json", tokens);
}

main();
