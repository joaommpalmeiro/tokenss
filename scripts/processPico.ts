import path from "node:path";
import is from "@sindresorhus/is";
import { composeVisitors, transform } from "lightningcss";

import {
  extractRgbColor,
  initColors,
  readStylesheet,
  removePrefix,
  rgbToHex,
  writeTokens,
} from "./utils";

const SHEET = "@picocss/pico/css/pico.colors.css";
const PREFIX = "--pico-color-";

async function main() {
  const code = await readStylesheet(SHEET);
  const tokens = initColors();

  transform({
    filename: path.basename(SHEET),
    minify: false,
    sourceMap: false,
    code,
    visitor: composeVisitors([
      {
        Declaration: {
          custom(node) {
            const parsedName = removePrefix(node.name, PREFIX).split("-");
            const shade = parsedName.at(-1);
            const rgbColor = extractRgbColor(node);

            if (is.numericString(shade) && rgbColor) {
              const hexColor = rgbToHex(rgbColor);
              const palette = parsedName.slice(0, -1).join("-");

              tokens.colors[palette] = {
                ...tokens.colors[palette],
                [shade]: { $value: hexColor },
              };
            }
          },
        },
      },
    ]),
  });

  await writeTokens("pico.tokens.json", tokens);
}

main();
