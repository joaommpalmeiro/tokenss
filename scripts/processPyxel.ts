import path from "node:path";
import { composeVisitors, transform } from "lightningcss";

import {
  extractRgbColor,
  fetchStylesheet,
  initColors,
  removePrefix,
  rgbToHex,
  writeTokens,
} from "./utils";

const SHEET = new URL("https://raw.githubusercontent.com/kitao/pyxel/v2.2.0/wasm/pyxel.css");
const PREFIX = "--pyxel-";

async function main() {
  const code = await fetchStylesheet(SHEET);
  const tokens = initColors();

  transform({
    filename: path.basename(SHEET.pathname),
    minify: false,
    sourceMap: false,
    code,
    visitor: composeVisitors([
      {
        Declaration: {
          custom(node) {
            if (node.name.startsWith(PREFIX) && !node.name.endsWith("background-color")) {
              const parsedName = removePrefix(node.name, PREFIX);
              const rgbColor = extractRgbColor(node);

              if (rgbColor) {
                const hexColor = rgbToHex(rgbColor);

                tokens.colors[parsedName] = { $value: hexColor };
              }
            }
          },
        },
      },
    ]),
  });

  await writeTokens("pyxel.tokens.json", tokens);
}

main();
