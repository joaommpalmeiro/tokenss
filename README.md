# tokenss

Design tokens from different projects as Design Tokens Community Group's JSON (DTCG format).

- [Source code](https://gitlab.com/joaommpalmeiro/tokenss)
- [npm package](https://www.npmjs.com/package/tokenss)
- [Licenses](https://licenses.dev/npm/tokenss/0.1.0)

## Available tokens

| Project                                 | Version                                              | File                                          |
| --------------------------------------- | ---------------------------------------------------- | --------------------------------------------- |
| [Pico](https://picocss.com/)            | [2.0.6](https://github.com/picocss/pico/tree/v2.0.6) | [pico.tokens.json](./dtcg/pico.tokens.json)   |
| [Pyxel](https://github.com/kitao/pyxel) | [2.2.0](https://github.com/kitao/pyxel/tree/v2.2.0)  | [pyxel.tokens.json](./dtcg/pyxel.tokens.json) |

## Development

Install [fnm](https://github.com/Schniz/fnm) (if necessary).

```bash
fnm install && fnm use && node --version && npm --version
```

```bash
npm install
```

```bash
npm run format
```

```bash
npm run lint
```

## Deployment

```bash
npm pack --dry-run
```

```bash
npm version patch
```

```bash
npm version minor
```

```bash
npm version major
```

- Update the version in the `Licenses` link at the top.

```bash
echo "v$(npm pkg get version | tr -d \")" | pbcopy
```

- Commit and push changes.
- Create a tag on [GitHub Desktop](https://github.blog/2020-05-12-create-and-push-tags-in-the-latest-github-desktop-2-5-release/).
- Check [GitLab](https://gitlab.com/joaommpalmeiro/tokenss/-/tags).

```bash
npm login
```

```bash
npm publish
```

- Check [npm](https://www.npmjs.com/package/tokenss).
