import { readFile, writeFile } from "node:fs/promises";
import path from "node:path";
import type { Group } from "@cobalt-ui/core";
import Color from "colorjs.io";
import type { RGBColor } from "lightningcss";

export async function readStylesheet(stylesheet: string): Promise<Buffer> {
  const inputPath = new URL(path.join("../node_modules", stylesheet), import.meta.url);
  const content = await readFile(inputPath);

  return content;
}

export async function writeTokens(outputFilename: string, tokens: Group): Promise<void> {
  const outputPath = new URL(path.join("../dtcg", outputFilename), import.meta.url);
  await writeFile(outputPath, JSON.stringify(tokens));
}

export function rgbToHex(color: RGBColor): string {
  const { r, g, b, alpha } = color;
  const parsedColor = new Color(`rgba(${r} ${g} ${b} / ${alpha})`);

  return parsedColor.toString({ format: "hex", collapse: false });
}

export function removePrefix(value: string, prefix: string): string {
  return value.startsWith(prefix) ? value.slice(prefix.length) : value;
}
