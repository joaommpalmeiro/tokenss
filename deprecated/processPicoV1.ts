import type { Group } from "@cobalt-ui/core";
import is from "@sindresorhus/is";
import { transform } from "lightningcss";

import { readStylesheet, removePrefix, rgbToHex, writeTokens } from "./utils";

async function main() {
  const colors = await readStylesheet("@picocss/pico/css/pico.colors.css");
  const tokens: Group = {};

  transform({
    filename: "pico.colors.css",
    minify: false,
    sourceMap: false,
    code: colors,
    visitor: {
      // biome-ignore lint/style/useNamingConvention: follow the Lightning CSS API
      Rule: {
        style(rule) {
          if (
            rule.value.selectors.length === 1 &&
            rule.value.selectors[0].length === 1 &&
            rule.value.selectors[0][0].type === "pseudo-class" &&
            rule.value.selectors[0][0].kind === "root"
          ) {
            for (const declaration of rule.value.declarations.declarations) {
              if (
                declaration.property === "custom" &&
                declaration.value.value.length === 1 &&
                declaration.value.value[0].type === "color" &&
                is.object(declaration.value.value[0].value) &&
                declaration.value.value[0].value.type === "rgb"
              ) {
                const parsedName = removePrefix(declaration.value.name, "--pico-color-").split("-");
                const shade = parsedName.at(-1);

                if (is.numericString(shade)) {
                  const hexColor = rgbToHex(declaration.value.value[0].value);
                  const palette = parsedName.slice(0, -1).join("-");

                  tokens[palette] = {
                    ...tokens[palette],
                    [shade]: { $type: "color", $value: hexColor },
                  };
                }
              }
            }
          }
        },
      },
    },
  });

  await writeTokens("pico.tokens.json", tokens);
}

main();
