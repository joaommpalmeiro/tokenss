# Notes

- https://github.com/joaopalmeiro/template-ts-package
- https://gitlab.com/joaommpalmeiro/resetss
- https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties: `Custom properties (sometimes referred to as CSS variables or cascading variables) (...)`
- https://tailwindcss.com/docs/adding-custom-styles: `(...) your design tokens (...)`
- https://nucleoapp.com/svg-flag-icons
- https://www.figma.com/community/plugin/1067561134666722782/palette-importer
- https://github.com/picocss/pico/blob/v2.0.6/css/pico.colors.css
- Cobalt/Terrazzo and DTCG tokens:
  - DTCG: Design Tokens Community Group
  - https://tr.designtokens.org/format/
  - https://github.com/design-tokens/community-group/
  - https://cobalt-ui.pages.dev/
  - https://cobalt-ui.pages.dev/guides/linting:
    - https://cobalt-ui.pages.dev/guides/linting#built-in-rules
    - https://cobalt-ui.pages.dev/integrations/a11y
  - https://github.com/terrazzoapp/terrazzo
  - https://terrazzo.app/docs/
  - https://www.figma.com/community/plugin/1254538877056388290/tokensbrucke
  - https://github.com/terrazzoapp/terrazzo/issues/201
  - https://terrazzo.app/docs/reference/tokens/
  - https://github.com/penpot/penpot-export
  - https://cobalt-ui.pages.dev/guides/cli
  - https://cobalt-ui.pages.dev/advanced/config#npm:
    - "To load tokens from an npm package, update `config.tokens` to point to the full JSON path (not merely the root package)"
  - [Multiple output files / omitting tokens from output in specific plugins](https://github.com/terrazzoapp/terrazzo/issues/250) issue
  - https://terrazzo.app/docs/cli/integrations/css/#css-color-module-4-support
- https://github.com/lukasoppermann/style-dictionary-utils:
  - https://github.com/lukasoppermann/style-dictionary-utils/tree/v4-pre-release.1?tab=readme-ov-file#w3ctokenjsonparser
- `Design tokens from different projects as CSS variables.`
- https://marketplace.visualstudio.com/items?itemName=vunguyentuan.vscode-css-variables
- https://jsonhero.io/
- https://gorzelinski.com/blog/converting-design-tokens-to-css-variables-with-node.js/
- https://www.supernova.io/
- https://specifyapp.com/
- https://github.com/salesforce-ux/theo
- https://fullystacked.net/design-tokens/:
  - https://github.com/AnimaApp/design-token-validator:
    - https://animaapp.github.io/design-token-validator-site/
  - https://github.com/universal-design-tokens/udt
  - https://github.com/Heydon/design-tokens-cli
- https://github.com/amzn/style-dictionary
- https://github.com/nl-design-system/utrecht:
  - https://www.npmjs.com/package/@utrecht/design-tokens
- https://www.bounteous.com/insights/2019/03/22/orange-you-accessible-mini-case-study-color-ratio
- https://docs.deno.com/runtime/manual/advanced/jsx_dom/css
- https://github.com/deno-front-end/css-parser
- https://design-tokens.github.io/community-group:
  - https://design-tokens.github.io/community-group/format/#file-extensions: `.tokens` or `.tokens.json`
  - https://design-tokens.github.io/community-group/format/#color
- https://github.com/terrazzoapp/terrazzo/blob/%40cobalt-ui/core%401.11.2/packages/core/src/index.ts#L74:
  - `/** The user's original schema, as-authored */`
  - `rawSchema: Group;`
  - https://github.com/terrazzoapp/terrazzo/blob/%40cobalt-ui/core%401.11.2/packages/core/src/token.ts#L3
- https://arktype.io/
- https://github.com/sinclairzx81/typebox
- https://valibot.dev/
- https://github.com/sindresorhus/is
- https://github.com/parcel-bundler/lightningcss/blob/v1.25.1/scripts/build-prefixes.js#L331:
  - https://github.com/mdn/browser-compat-data
- https://github.com/color-js/color.js/blob/v0.5.1/src/spaces/srgb.js#L84
- [How library and tooling authors do keep track of conformance and changes?](https://github.com/design-tokens/community-group/issues/247) issue:
  - https://github.com/nclsndr/design-tokens-format-module
  - https://github.com/AnimaApp/design-token-validator
  - https://animaapp.github.io/design-token-validator-site/
- https://styledictionary.com/info/tokens/:
  - https://tr.designtokens.org/format/#types
  - https://styledictionary.com/reference/types/
  - https://styledictionary.com/reference/utils/tokens/
  - https://styledictionary.com/reference/utils/dtcg/
  - https://styledictionary.com/info/dtcg/:
    - "Converts `value`, `type` and `description` design token property keys into `$value`, `$type` and `$description` respectively."
    - "Moves `$type` properties from the uppermost common ancestor token group to individual design tokens."
- https://styledictionary.com/reference/api/
- https://github.com/color-js/color.js/releases
- https://github.com/amzn/style-dictionary/tree/v4.0.1/examples/basic/tokens
- https://styledictionary.com/reference/utils/format-helpers/
- https://code.visualstudio.com/updates/v1_79#_readonly-mode
- https://github.com/amzn/style-dictionary/blob/v4.0.1/types/DesignToken.ts
- https://github.com/amzn/style-dictionary/blob/v4.0.1/lib/utils/detectDtcgSyntax.js#L15-L39
- https://lightningcss.dev/transforms.html#value-types: "Unknown properties, including custom properties, have the property type `"custom"`. (...) To visit custom properties, use the `custom` visitor function (...)"
- https://www.typescriptlang.org/docs/handbook/declaration-files/do-s-and-don-ts.html#general-types: "Instead of `Object`, use the non-primitive `object` type (added in TypeScript 2.2)."
- https://tr.designtokens.org/format/#name-and-value: "Token names are case-sensitive (...)" + `"font-size"` + `"token name"`
- https://tr.designtokens.org/format/#dimension:
  - "Represents an amount of distance in a single dimension in the UI, such as a position, width, height, radius, or thickness."
  - "(...) followed by either a "px" or "rem" unit (...)"
- https://github.com/tailwindlabs/tailwindcss/blob/v3.4.10/stubs/config.full.js
- https://github.com/tailwindlabs/tailwindcss/blob/v3.4.10/src/public/default-theme.js
- https://tr.designtokens.org/format/#type-0:
  - "(...) if any of the token's parent groups have a `$type` property, then the token's type is inherited from the closest parent group with a `$type` property."
  - "The `$type` property can be set on different levels:"
    - "at the group level"
    - "at the token level"
  - "The value of `$type` is case-sensitive."
- https://tailwindcss.com/docs/border-radius
- [Need way to express hybrid types that are indexable for a subset of properties](https://github.com/microsoft/TypeScript/issues/17867) open issue
- https://github.com/basarat/typescript-book
- https://github.com/justinlettau/ts-dot-prop
- https://github.com/justinlettau/ts-util-is
- https://github.com/jonschlinkert/set-value/blob/4.1.0/index.js#L131-L161

## Commands

```bash
npm install -D \
@biomejs/biome \
@picocss/pico \
@sindresorhus/is \
colorjs.io \
lightningcss \
npm-run-all2 \
sort-package-json \
tailwindcss \
tsx
```

```bash
rm -rf node_modules/ && npm install
```

```bash
npm install -D "@types/node@$(cat .nvmrc | cut -d . -f 1-2)"
```

```bash
npm install -D @cobalt-ui/cli @cobalt-ui/core
```

```bash
npx co check dtcg/pico.tokens.json
```

```bash
npx co build
```

```bash
npx co convert test.json --out testOutput.json
```

```bash
lychee README.md NOTES.md --verbose
```

```bash
npm install -D @animaapp/design-tokens-validator
```

## Snippets

```js
import pluginCSS from "@cobalt-ui/plugin-css";

/** @type {import("@cobalt-ui/core").Config} */
export default {
  plugins: [
    pluginCSS({
      p3: false,
      colorFormat: "none",
    }),
  ],
  tokens: ["./tokens/pico.json"],
  lint: {
    rules: {
      "duplicate-values": "error",
      naming: ["error", { format: "kebab-case" }],
    },
  },
};
```

### Style Dictionary vs. DTCG

```json
{
  "colors": {
    "font": {
      "base": { "value": "#111111", "type": "color" },
      "secondary": { "value": "#333333", "type": "color" },
      "tertiary": { "value": "#666666", "type": "color" },
      "inverse": {
        "base": { "value": "#ffffff", "type": "color" }
      }
    }
  }
}
```

```json
{
  "colors": {
    "$type": "color",
    "font": {
      "base": { "$value": "#111111" },
      "secondary": { "$value": "#333333" },
      "tertiary": { "$value": "#666666" },
      "inverse": {
        "base": { "$value": "#ffffff" }
      }
    }
  }
}
```

- https://styledictionary.com/info/dtcg/

```json
{
  "colors": {
    "$type": "color",
    "black": {
      "$value": "#000000"
    },
    "white": {
      "$value": "#ffffff"
    },
    "orange": {
      "100": {
        "$value": "#fffaf0"
      },
      "200": {
        "$value": "#feebc8"
      },
      "300": {
        "$value": "#fbd38d"
      },
      "400": {
        "$value": "#f6ad55"
      },
      "500": {
        "$value": "#ed8936"
      },
      "600": {
        "$value": "#dd6b20"
      },
      "700": {
        "$value": "#c05621"
      },
      "800": {
        "$value": "#9c4221"
      },
      "900": {
        "$value": "#7b341e"
      }
    }
  },
  "dimensions": {
    "0": {
      "$value": "0px"
    },
    "1": {
      "$value": "4px"
    },
    "2": {
      "$value": "8px"
    },
    "3": {
      "$value": "12px"
    },
    "4": {
      "$value": "16px"
    },
    "5": {
      "$value": "20px"
    },
    "6": {
      "$value": "24px"
    },
    "7": {
      "$value": "28px"
    },
    "8": {
      "$value": "32px"
    },
    "9": {
      "$value": "36px"
    },
    "10": {
      "$value": "40px"
    },
    "11": {
      "$value": "44px"
    },
    "12": {
      "$value": "48px"
    },
    "13": {
      "$value": "52px"
    },
    "14": {
      "$value": "56px"
    },
    "15": {
      "$value": "60px"
    },
    "$type": "dimension",
    "max": {
      "$value": "9999px"
    }
  },
  "text": {
    "fonts": {
      "$type": "fontFamily",
      "serif": {
        "$value": "Times New Roman, serif"
      },
      "sans": {
        "$value": "Open Sans, sans-serif"
      }
    },
    "weights": {
      "$type": "fontWeight",
      "light": {
        "$value": "thin"
      },
      "regular": {
        "$value": "regular"
      },
      "bold": {
        "$value": "extra-bold"
      }
    },
    "lineHeights": {
      "$type": "number",
      "normal": {
        "$value": 1.2
      },
      "large": {
        "$value": 1.8
      }
    },
    "typography": {
      "$type": "typography",
      "heading": {
        "$value": {
          "fontFamily": "{text.fonts.sans}",
          "fontWeight": "{text.weights.bold}",
          "fontSize": "{dimensions.7}",
          "lineHeight": "{text.lineHeights.large}"
        }
      },
      "body": {
        "$value": {
          "fontFamily": "{text.fonts.serif}",
          "fontWeight": "{text.weights.regular}",
          "fontSize": "{dimensions.4}",
          "lineHeight": "{text.lineHeights.normal}"
        }
      }
    }
  },
  "transitions": {
    "$type": "transition",
    "emphasis": {
      "$value": {
        "duration": "{transitions.durations.medium}",
        "delay": "{transitions.durations.instant}",
        "timingFunction": "{transitions.easingFunctions.accelerate}"
      }
    },
    "fade": {
      "$value": {
        "duration": "{transitions.durations.long}",
        "delay": "{transitions.durations.instant}",
        "timingFunction": "{transitions.easingFunctions.decelerate}"
      }
    },
    "easingFunctions": {
      "$type": "cubicBezier",
      "accelerate": {
        "$value": [0.5, 0, 1, 1]
      },
      "decelerate": {
        "$value": [0, 0, 0.5, 1]
      }
    },
    "durations": {
      "$type": "duration",
      "instant": {
        "$value": "0ms"
      },
      "short": {
        "$value": "100ms"
      },
      "medium": {
        "$value": "300ms"
      },
      "long": {
        "$value": "600ms"
      }
    }
  },
  "borders": {
    "$type": "border",
    "heavy": {
      "$value": {
        "color": "{colors.black}",
        "width": "{dimensions.1}",
        "style": "{borders.styles.solid}"
      }
    },
    "wireframe": {
      "$value": {
        "color": "{colors.orange.600}",
        "width": "{dimensions.2}",
        "style": "{borders.styles.dashed}"
      }
    },
    "styles": {
      "$type": "strokeStyle",
      "solid": {
        "$value": "solid"
      },
      "dashed": {
        "$value": {
          "dashArray": ["0.5rem", "0.25rem"],
          "lineCap": "round"
        }
      }
    }
  },
  "shadows": {
    "$type": "shadow",
    "sm": {
      "$value": {
        "color": "{colors.black}",
        "offsetX": "{dimensions.0}",
        "offsetY": "{dimensions.1}",
        "blur": "{dimensions.3}"
      }
    },
    "lg": {
      "$value": {
        "color": "{colors.black}",
        "offsetX": "{dimensions.0}",
        "offsetY": "{dimensions.2}",
        "blur": "{dimensions.4}"
      }
    },
    "multi": {
      "$value": ["{shadows.sm}", "{shadows.lg}"]
    }
  }
}
```

### `package.json` file

```json
{
  "scripts": {
    "dev:pico": "tsx scripts/processPico.ts",
    "format": "run-s format:code format:pkgJson",
    "format:code": "biome check --apply .",
    "format:pkgJson": "sort-package-json",
    "lint": "run-s lint:pkgJson lint:code lint:dtcg",
    "lint:code": "biome lint .",
    "lint:dtcg": "co check",
    "lint:pkgJson": "sort-package-json --check"
  },
  "devDependencies": {
    "@biomejs/biome": "1.6.3",
    "@cobalt-ui/cli": "1.11.2",
    "@cobalt-ui/core": "1.11.2",
    "@picocss/pico": "2.0.6",
    "@sindresorhus/is": "6.3.1",
    "@types/node": "18.19.39",
    "colorjs.io": "0.5.2",
    "lightningcss": "1.25.1",
    "npm-run-all2": "6.1.2",
    "sort-package-json": "2.10.0",
    "tsx": "4.15.6"
  }
}
```

### Lightning CSS

- https://lightningcss.dev/transforms.html#composing-visitors

```ts
import { transform, composeVisitors } from "lightningcss";

let environmentVisitor = {
  EnvironmentVariable: {
    "--branding-padding": () => ({
      type: "length",
      value: {
        unit: "px",
        value: 20,
      },
    }),
  },
};

let doubleFunctionVisitor = {
  FunctionExit: {
    double(f) {
      if (f.arguments[0].type === "length") {
        return {
          type: "length",
          value: {
            unit: f.arguments[0].value.unit,
            value: f.arguments[0].value.value * 2,
          },
        };
      }
    },
  },
};

let res = transform({
  filename: "test.css",
  minify: true,
  code: Buffer.from(`
    .foo {
      padding: double(env(--branding-padding));
    }
  `),
  visitor: composeVisitors([environmentVisitor, doubleFunctionVisitor]),
});
```

### TypeScript

- https://basarat.gitbook.io/typescript/type-system/index-signatures#using-a-limited-set-of-string-literals

```ts
type Index = "a" | "b" | "c";
type FromIndex = { [k in Index]?: number };

const good: FromIndex = { b: 1, c: 2 };
```

- https://basarat.gitbook.io/typescript/type-system/index-signatures#design-pattern-nested-index-signature

```ts
interface NestedCSS {
  color?: string;
  nest?: {
    [selector: string]: NestedCSS;
  };
}

const example: NestedCSS = {
  color: "red",
  nest: {
    ".subclass": {
      color: "blue",
    },
  },
};

const failsSilently: NestedCSS = {
  colour: "red", // TS Error: unknown property `colour`
};
```

- https://basarat.gitbook.io/typescript/type-system/discriminated-unions#throw-in-exhaustive-checks

```ts
function assertNever(x: never): never {
  throw new Error("Unexpected value. Should have been never.");
}

function area(s: Shape) {
  switch (s.kind) {
    case "square":
      return s.size * s.size;
    case "rectangle":
      return s.width * s.height;
    default:
      return assertNever(s);
  }
}
```

- https://www.emmanuelgautier.com/blog/snippets/typescript-required-properties
- https://github.com/sindresorhus/type-fest/blob/v4.25.0/source/set-required.d.ts
- https://github.com/millsp/ts-toolbelt/blob/v9.5.1/sources/Object/Optional.ts

```ts
type Required<T> = {
  [P in keyof T]-?: T[P];
};

type WithRequired<T, K extends keyof T> = T & { [P in K]-?: T[P] };
```

### `setNested()` helper function

```ts
export function setNested(obj: object, keys: string[], value: any): object {
  const [first, ...rest] = keys;

  if (rest.length === 0) {
    return { ...obj, [first]: value };
  }

  return {
    ...obj,
    [first]: setNested(obj[first] ?? {}, rest, value),
  };
}
```

```ts
function setNested<T extends object>(obj: T, keys: string[], value: any): T {
  const result = { ...obj };
  let current = result;

  for (let i = 0; i < keys.length - 1; i++) {
    const key = keys[i];
    if (!(key in current)) {
      current[key] = {};
    }
    current = current[key];
  }

  current[keys.length - 1] = value;

  return result;
}
```

```ts
function setNested<T extends object>(obj: T, keys: string[], value: any): T {
  const result = structuredClone(obj);
  let current = result;

  for (let i = 0; i < keys.length - 1; i++) {
    const key = keys[i];
    if (!(key in current)) {
      current[key] = {};
    }
    current = current[key];
  }

  current[keys.length - 1] = value;

  return result;
}
```
